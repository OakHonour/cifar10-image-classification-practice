import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler
import numpy as np
import argparse
from model import CNN
from utils import Model


def read_args():
    parser = argparse.ArgumentParser(description='Run test validation')
    parser.add_argument('-t', '--test_or_train', required=True, help='Test or train')
    parser.add_argument('-i', '--input', required=False, help='Checkpoint file')
    args = parser.parse_args()

    return vars(args)

if __name__ == '__main__':
    args = read_args()
    path = args['input']
    model = Model(0, 20, CNN(), epochs=30, checkpoint_path=path)

    if args['test_or_train'] == 'train':
        model.train_model()
    if args['test_or_train'] == 'test':
        model.test_model()

    

