import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler
import numpy as np


class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()

        # begin with an image deptch of three for RGB
        self.conv1 = nn.Conv2d(3, 16,kernel_size=3,padding=1)
        # res decreases and deptch decreases each layer
        self.conv2 = nn.Conv2d(16, 32,kernel_size=3,padding=1)

        self.conv3 = nn.Conv2d(32, 64,kernel_size=3,padding=1)

        self.conv4 = nn.Conv2d(64, 128,kernel_size=3,padding=1)
        # output of 2 * 2 * 128

        self.conv5 = nn.Conv2d(128, 256,kernel_size=3,padding=1)
        # output is 1 * 1 * 256
        

        self.pool = nn.MaxPool2d(2,2)

        self.fc1 = nn.Linear(256, 500)

        self.fc2 = nn.Linear(500, 10)

        self.dropout = nn.Dropout(0.25)

    def forward(self, x):
        # relu is activation function to convert scoes into probabilites
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = self.pool(F.relu(self.conv3(x)))
        x = self.pool(F.relu(self.conv4(x)))
        x = self.pool(F.relu(self.conv5(x)))
        x = x.view(-1, 256)

        x = self.dropout(x)

        x = F.relu(self.fc1(x))

        x = self.dropout(x)

        x = self.fc2(x)

        return x