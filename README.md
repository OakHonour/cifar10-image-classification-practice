# cifar10 Image Classification Practice

An image classification model written as part of the Udacity PyTorch course: https://classroom.udacity.com/courses/ud188.
Mostly used for experimenting and a reference.

### Usage

To train:

``python classifer.py -t train``

to test:

``python classifier.py -t test -i path/to/checkpoint.pth``

### To do

- Code clean and commenting
- A global_vars.py for easy access to key variables
