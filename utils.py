import torch
import torch.optim as optim
import torch.nn as nn
from model import CNN
from torch.utils.data.sampler import SubsetRandomSampler
import torchvision
import torchvision.transforms as transforms
import numpy as np

class Model():
    def __init__(self, num_workers=0, batch_size=20, Model=CNN(), epochs=30, checkpoint_path=None):
        self.num_workers = num_workers # 0
        self.batch_size = batch_size # 20
        self.model = Model # CNN()
        self.epochs = epochs
        self.checkpoint_path = checkpoint_path

    def train_model(self):
        num_workers = self.num_workers
        batch_size = self.batch_size
        # we can take a percentage of the traninig set to use for validation
        val_size = 0.2 # 20%

        #device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        #print(device)

        transform = transforms.Compose([transforms.ToTensor(),
                                        transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))])

        train_data = torchvision.datasets.CIFAR10('data', train=True, download=True, transform=transform)

        num_train = len(train_data)
        # return a list of the training data index
        indices = list(range(num_train))
        np.random.shuffle(indices)
        # split will contain the index 20% into the training set - in this case index 10000
        split = int(np.floor(val_size * num_train))
        # create two new list which contain the indices before and after the split
        train_idx, valid_idx = indices[split:], indices[:split]

        # create data samplers for train and val data
        train_sampler = SubsetRandomSampler(train_idx)
        val_sampler = SubsetRandomSampler(valid_idx)

        train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size,
            sampler=train_sampler, num_workers=num_workers)
        val_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, 
            sampler=val_sampler, num_workers=num_workers)

        model = self.model

        print(model)

        model.cuda()
        criterion = nn.CrossEntropyLoss()

        optimiser = optim.SGD(model.parameters(), lr=0.01)

        ### The training loop

        epochs = self.epochs

        min_val_loss = np.Inf

        for epoch in range(epochs):

            print("Epoch: {}".format(epoch))

            train_loss = 0.0
            val_loss = 0.0

            # Set model to training mode
            model.train()

            for image, label in train_loader:
                image = image.cuda()
                label = label.cuda()

                optimiser.zero_grad()
                # get out prediction
                output = model(image)
                # evaluate loss against label to see how far away from the actual result our prediction is
                loss = criterion(output, label)
                # compute the gradient of the loss relative to the model weights and discover which weights are responsible for any errors
                loss.backward()
                # use an optimiser such as SGD to clculate a better weight value
                optimiser.step()

                train_loss += loss.item() * image.size(0)

            # Establish validation loss
            for image, label in val_loader:
                image = image.cuda()
                label = label.cuda()

                output = model(image)

                # evaluate loss against label
                loss = criterion(output, label)
                val_loss += loss.item() * image.size(0)

            train_loss = train_loss/len(train_loader.sampler)
            val_loss = val_loss/len(val_loader.sampler)

            print("Completed Epoch {} Training Loss: {:.6f}. Val Loss: {:.6f}".format(epoch, train_loss, val_loss))


            if val_loss < min_val_loss:
                
                print("Validation loss has decreased from {:.6f} to {:.6f}. Saving checkpoint file.".format(min_val_loss, val_loss))
                min_val_loss = val_loss

                torch.save(model.state_dict(), 'checkpoints/val_loss_{:.6f}.pth'.format(val_loss))

    def test_model(self):
        num_workers = self.num_workers
        batch_size = self.batch_size

        transform = transforms.Compose([transforms.ToTensor(),
                                    transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))])
        # create our model instance
        model = self.model
        # load in our weights and biases
        model.load_state_dict(torch.load(self.checkpoint_path))
        # set to eval mode
        model.eval()
        model.cuda()
        criterion = nn.CrossEntropyLoss()

        classes = ['airplane', 'automobile', 'bird', 'cat', 'deer',
            'dog', 'frog', 'horse', 'ship', 'truck']

        test_data = torchvision.datasets.CIFAR10('data', train=False, download=True, transform=transform)
        test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, 
            num_workers=num_workers)

        test_loss = 0.0
        class_correct = list(0. for i in range(10))
        class_total = list(0. for i in range(10))

       

        for image, label in test_loader:
            image = image.cuda()
            label = label.cuda()

            output = model(image)

            loss = criterion(output, label)

            test_loss += loss.item() * image.size(0)

            blank, pred = torch.max(output, 1)

            correct_tensor = pred.eq(label.data.view_as(pred))

            correct = np.squeeze(correct_tensor.cpu().numpy())

            for i in range(batch_size):
                class_label = label.data[i]
                class_correct[class_label] += correct[i].item()
                class_total[class_label] += 1
            
        test_loss = test_loss/len(test_loader.dataset)
        print('Test Loss: {:.6f}\n'.format(test_loss))   
            
        for i in range(10):
            if class_total[i] > 0:
                print('Test Accuracy of %5s: %2d%% (%2d/%2d)' % (
                    classes[i], 100 * class_correct[i] / class_total[i],
                    np.sum(class_correct[i]), np.sum(class_total[i])))
            else:
                print('Test Accuracy of %5s: N/A (no training examples)' % (classes[i]))

        print('\nTest Accuracy (Overall): %2d%% (%2d/%2d)' % (
            100. * np.sum(class_correct) / np.sum(class_total),
            np.sum(class_correct), np.sum(class_total)))
                